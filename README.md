# Introduction
Qwilfish is a Python package for fuzzing various Ethernet-related protocols.
It is a work in progress and the first goal is grammar-based generation of
LLDP frames (IEEE 802.1AB).

[[_TOC_]]

# Installation

## Prerequisites
- A Linux system
- Python3.8 (higher versions will probably work too, but no guarantees)
- Root privileges (for changing the capabilities of Python binary)

## Create a virtual environment
It is recommended to create a virtual environment first:
```
$ python -m venv venv
$ source venv/bin/activate
```

## Install with pip
To install Qwilfish simply type:
```
$ pip install qwilfish
```

## Install from source
To install Qwilfish from source type:
```
$ git clone https://gitlab.com/zluudg/qwilfish.git
$ cd qwilfish
$ pip install .
```

Qwilfish also supports an editable install:
```
$ pip install -e .
```

## Setting capabilities
Qwilfish writes packets to raw sockets, which is prohibited for normal users.
It is not recommended to install or run Qwilfish as root, however.
Instead, change the capabilities of your Python binary:
```
$ sudo setcap cap_net_raw=eip /path/to/python/binary
```

# Usage
## Basic usage
Qwilfish can be invoked without any commands:
```
$ qwilfish
```
It will then look for a configuration in the following locations and then run
according to the first one it finds:
1. `<current directory>/default.yaml`
2. `<user homedir>/.qwilfish/config/default.yaml`
3. `<qwilfish root>/src/qwilfish/configuration/default.yaml`

To use another configuration file instead of `default.yaml` use the `-C` flag.
```
$ qwilfish -C custom_config.yaml
```
It will be searched for under the following directories with priority as
listed:
1. `<current directory>/`
2. `<user homedir>/.qwilfish/config/`
3. `<qwilfish root>/src/qwilfish/configuration/`

Don't run any tests. Instead create the folder structure for plugins and
configuration in user's homedir.
```
$ qwilfish -I
```
The generated structure will be the following:
```
<user homedir>/
|-- .qwilfish/
    |-- config/
    `-- plugins/
        |-- arbiter/
        |-- courier/
        |-- grammar/
        `-- reward_shaper/
```

Set logging level to DEBUG:
```
$ qwilfish -d
```

Print help:
```
$ qwilfish -h
```

Don't create a database for the test results (useful when developing plugins
and testing configuration):
```
$ qwilfish -n
```

Choose a custom name for the results database:
```
$ qwilfish -o some_database_name.db
```

## Advanced Configuration
There are some possibilities to configure a Qwilfish session beyond what is
offered by the CLI. Please refer to
[this guide](src/qwilfish/configuration/README.md) for more info.

## Writing Plugins
Certain components in Qwilfish can be replaced in a plugin fashion. For more
info check out [this guide](src/qwilfish/plugins/README.md).

# Credit
This project is more than heavily inspired by
[The Fuzzing Book](https://www.fuzzingbook.org/). Be sure to check it out!
