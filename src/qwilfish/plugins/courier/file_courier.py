# Standard lib imports
import platform

# Local imports
from qwilfish.session_builder import register_courier

def initialize():
    '''Registers FileCourier as a plugin.'''
    register_courier(FileCourier.PLUGIN_IDENTIFIER, FileCourier)

class FileCourier:
    '''Writes fuzz data to a file.'''

    # Plugin identifier
    PLUGIN_IDENTIFIER = "file_courier"

    def __init__(self, filename="file_courier.txt"):
        if platform.system() != "Linux":
            raise NotImplementedError(
                f"Not supported on {platform.system()}")
        else:
            self.filename = filename

    def init(self):
        self.outfile = open(self.filename, "w")

    def deliver(self, data):
        self.outfile.write(data + "\n")
        return True

    def destroy(self):
        self.outfile.close()
