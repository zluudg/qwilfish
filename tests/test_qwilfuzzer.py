'''
Unit tests for the main functionality of the  qwilfuzzer module.
'''

# Standard library imports
import unittest

# Local imports
import qwilfish.qwilfuzzer as qqf

class TestQwilfuzzerDummy(unittest.TestCase):
    '''
    Dummy test class
    '''

    def test_dummy(self):
        '''
        Dummy test
        '''
        self.assertTrue(True)

if __name__ == "__main__":
    unittest.main()
